# CPP - TP5

## Before

```
Finished calculation (1000) in avg.	102ms, total	513ms
Finished calculation (30000) in avg.	24987ms, total	124937ms
Finished calculation (30000) in avg.	27633ms, total	138168ms
Finished calculation (30000) in avg.	47920ms, total	239604ms
```

## Valgrind

### Before memory leak correction

[Valgrind report before correction](https://gitlab.forge.hefr.ch/nael.telfser/cpp-tp-4/-/blob/master/valgrind.xml)

```
Finished calculation (1000) in avg.	59ms, total	296ms
```

### After memory leak correction

[Valgrind report after correction](https://gitlab.forge.hefr.ch/nael.telfser/cpp-tp-4/-/blob/master/valgrind_after.xml)

```
Finished calculation (1000) in avg.	56ms, total	284ms
```

## Optimisation flags

### Before

```
Finished calculation (1000) in avg.	56ms, total	284ms
```

### After

```
Finished calculation (1000) in avg.	20ms, total	103ms
```

## Vectorization

### Before

```
Finished calculation (1000) in avg.	20ms, total	103ms
```

### After

```
Finished calculation (1000) in avg.	8ms, total	43ms
```

## After

```
Finished calculation (1000) in avg.	14ms, total	72ms
Finished calculation (30000) in avg.	1987ms, total	9935ms
Finished calculation (30000) in avg.	2364ms, total	11820ms
Finished calculation (30000) in avg.	3224ms, total	16123ms
```

## GPrefTools

Etant sur mac, je n'ai pas réussi à générer le rapport avec les noms des objets. Comme mentionné par l'enseignant, cette partie pouvait être ignorée.

## Conclusion

Après quelques optimisations, le programme est environ 15 fois plus rapide.