//
// Created by beatw on 3/3/2022.
//


#include "../includes/Catch2/catch.hpp"
#include "../src/Point.h"

TEST_CASE("Point: Construction") {
    Point p(10, 12);

    REQUIRE(10 == Approx(p.getX()));
    REQUIRE(12 == Approx(p.getY()));
    REQUIRE("" == p.getName());

    p = Point(-123, 234, "test");
    REQUIRE(-123 == Approx(p.getX()));
    REQUIRE(234 == Approx(p.getY()));
    REQUIRE("test" == p.getName());
}

TEST_CASE("Point: Construction 2") {
    Point p(10, 12);

    Point p2 = p;
    REQUIRE(p.getX() == Approx(p2.getX()));
    REQUIRE(p.getY() == Approx(p2.getY()));
    REQUIRE(p.getName() == p2.getName());
}


TEST_CASE("Point: Construction 3") {
    Point p(10, 12, "Name");

    Point p2 = p;
    REQUIRE(p.getX() == Approx(p2.getX()));
    REQUIRE(p.getY() == Approx(p2.getY()));
    REQUIRE(p.getName() == p2.getName());
}

TEST_CASE("Point: Chaining") {
    Point p(10, 12, "Name");

    (((p *= 10) /= 2) += {20, 10}) -= {5,3};

    REQUIRE(p.getX() == Approx(65));
    REQUIRE(p.getY() == Approx(67));
}

TEST_CASE("Point : getters") {
    Point p(10, 12, "Name");

    REQUIRE(p.getX() == Approx(10));
    REQUIRE(p.getY() == Approx(12));
    REQUIRE(p.getName() == "Name");
}

TEST_CASE("Point : setters") {
    Point p(10, 12, "Name");

    p.setX(20);
    p.setY(30);
    p.setName("NewName");

    REQUIRE(p.getX() == Approx(20));
    REQUIRE(p.getY() == Approx(30));
    REQUIRE(p.getName() == "NewName");
}

TEST_CASE("Point : Distance") {
    Point p1(10, 12, "Name");
    Point p2(20, 30, "Name");

    REQUIRE(p1.distance(p2) == Approx(20.591260282));
}

TEST_CASE("Point : Override operators") {
    Point p1(10, 12, "Name");
    Point p2(20, 30, "Name");

    p1 += p2;
    REQUIRE(p1.getX() == Approx(30));
    REQUIRE(p1.getY() == Approx(42));

    p1 -= p2;
    REQUIRE(p1.getX() == Approx(10));
    REQUIRE(p1.getY() == Approx(12));

    p1 *= 2;
    REQUIRE(p1.getX() == Approx(20));
    REQUIRE(p1.getY() == Approx(24));

    p1 /= 2;
    REQUIRE(p1.getX() == Approx(10));
    REQUIRE(p1.getY() == Approx(12));
    REQUIRE_THROWS(p1 /= 0);

    p1 = p1 + p2;
    REQUIRE(p1.getX() == Approx(30));
    REQUIRE(p1.getY() == Approx(42));

    p1 = p1 - p2;
    REQUIRE(p1.getX() == Approx(10));
    REQUIRE(p1.getY() == Approx(12));

    p1 = p1 * 2;
    REQUIRE(p1.getX() == Approx(20));
    REQUIRE(p1.getY() == Approx(24));

    p1 = p1 / 2;
    REQUIRE(p1.getX() == Approx(10));
    REQUIRE(p1.getY() == Approx(12));
    REQUIRE_THROWS(p1 = p1 / 0);
}
