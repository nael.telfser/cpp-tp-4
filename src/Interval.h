//
// Created by Beat Wolf on 30.11.2021.
//

#ifndef CPPALGO_INTERVAL_H
#define CPPALGO_INTERVAL_H

#include <limits>

template <typename T1 = int, typename T2 = double>
class Interval {
public:
    Interval(T1 start, T1 end, T2 value = 0);
    T1 getStart() const;
    T1 getEnd() const;
    T2 getValue() const;
    bool contains(T2 value) const;
    bool operator!=(const Interval& other);
    template <typename T_1, typename T_2>
    bool operator==(const Interval<T_1, T_2>& other) const {
        return start == other.start && end == other.end && value == other.value;
    }
    template <typename T>
    bool operator==(const Interval<T, double>& other) const {
        return start == other.start && end == other.end && std::numeric_limits<double>::epsilon() > std::abs(value - other.value);
    }
private:
    T1 start;
    T1 end;
    T2 value;
};

template <typename T1, typename T2>
Interval<T1, T2>::Interval(T1 start, T1 end, T2 value): start(start), end(end), value(value) {}

template <typename T1, typename T2>
T1 Interval<T1, T2>::getStart() const {
    return start;
}

template <typename T1, typename T2>
T1 Interval<T1, T2>::getEnd() const {
    return end;
}

template <typename T1, typename T2>
T2 Interval<T1, T2>::getValue() const {
    return value;
}

template <typename T1, typename T2>
bool Interval<T1, T2>::contains(T2 value) const {
    return value >= start && value <= end;
}

template <typename T1, typename T2>
bool Interval<T1, T2>::operator!=(const Interval& other) {
    return !(*this == other);
}

#endif //CPPALGO_INTERVAL_H
