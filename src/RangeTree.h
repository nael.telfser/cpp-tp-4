//
// Created by Beat Wolf on 27.06.2021.
//

#ifndef CPPALGO_RANGETREE_H
#define CPPALGO_RANGETREE_H

#include <vector>
#include <memory>
#include <numeric>
#include <algorithm>
#include "Point.h"
#include "base/BinaryTree.h"

class RangeTree{

public:
    /**
     * Build range tree given a list of points
     * @param points
     */
    RangeTree(std::vector<Point *> points);

    /**
     * Find all points in a given range (start to end)
     * @param start
     * @param end
     * @return
     */
    std::vector<Point *> search(const Point &start, const Point &end) const;

    /**
     * Return true if the RangeTree has no points
     * @return
     */
    bool isEmpty() const;

    /**
     * Return the amount of points in the RangeTree
     * @return
     */
    size_t size() const;

private:

    void search(std::vector<Point*> &result, BinaryTreeNode<BinaryTree<Point*, double>, Point*> *node, double xFrom, double xTo, double xMin, double xMax, double yFrom, double yTo) const;

    BinaryTree<BinaryTree<Point*, double>, Point*> build2DRangeTree(std::vector<Point*> &points, size_t left, size_t right) const;

    BinaryTree<BinaryTree<Point*, double>, Point*> xTree;
};


#endif //CPPALGO_RANGETREE_H
