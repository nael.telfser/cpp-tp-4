//
// Created by Beat Wolf on 30.11.2021.
//

#include "RangeTree.h"

RangeTree::RangeTree(std::vector<Point *> points) {
    //Sort all points by X
    std::sort(points.begin(), points.end(), [](Point *a, Point *b) {
        return a->getX() < b->getX();
    });

    //Build using all points (sorted in X)
    this->xTree = build2DRangeTree(points, 0, points.size());
}

BinaryTree<BinaryTree<Point *, double>, Point *> RangeTree::build2DRangeTree(std::vector<Point *> &points,
                                                                             size_t left,
                                                                             size_t right) const {
    BinaryTree<BinaryTree<Point *, double>, Point *> tx;

    if (left >= right) {
        return tx;
    }

    //Construct yTree
    std::vector<Point *> yPoints{points.begin() + left, points.begin() + right};
    BinaryTree<Point *, double> ty(yPoints,
                                   [](const Point *a) { return a->getY(); },
                                   true);

    //Construct xTree
    size_t mid = (left + right) / 2;
    tx.setRoot(points[mid], ty);

    //build left and right children
    tx.getRoot()->left = build2DRangeTree(points, left, mid).getRootCopy();
    tx.getRoot()->right = build2DRangeTree(points, mid + 1, right).getRootCopy();
    //WARNING: build2DRangeTree returns a temporary BinaryTree.
    //Use getRootCopy() to get a copy of the root node
    return tx;
}

std::vector<Point *> RangeTree::search(const Point &start, const Point &end) const {
    std::vector<Point *> result;
    search(result, xTree.getRoot(),
           start.getX(), end.getX(),
           std::numeric_limits<int>::min(), std::numeric_limits<int>::max(),
           start.getY(), end.getY());
    return result;
}

bool RangeTree::isEmpty() const {
    return xTree.isEmpty();
}

size_t RangeTree::size() const {
    return xTree.size();
}

void RangeTree::search(std::vector<Point *> &result,
                       BinaryTreeNode<BinaryTree<Point *, double>, Point *> *node,
                       double xFrom, double xTo,
                       double xMin, double xMax,
                       double yFrom, double yTo) const {

    if (node == nullptr) { // Bottom of the tree
        return;
    }

    auto valueX = node->key->getX();
    if (valueX > xTo) {
        search(result, node->left, xFrom, xTo, xMin, valueX, yFrom, yTo);
        return;
    } else if (valueX < xFrom) {
        search(result, node->right, xFrom, xTo, valueX, xMax, yFrom, yTo);
        return;
    }
    if (xMin >= xFrom && xMax <= xTo) {
        node->value.getRoot()->inRange(result, yFrom, yTo);
        return;
    }

    // Verify y coordinates
    auto valueY = node->key->getY();
    if (valueY >= yFrom && valueY <= yTo) {
        result.push_back(node->key);
    }

    // Search left and right subtrees
    search(result, node->left, xFrom, xTo, xMin, valueX, yFrom, yTo);
    search(result, node->right, xFrom, xTo, valueX, xMax, yFrom, yTo);
}

