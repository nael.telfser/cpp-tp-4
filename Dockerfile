FROM ubuntu:20.04

RUN DEBIAN_FRONTEND="noninteractive" apt-get update && apt-get -y install tzdata

RUN apt-get update \
  && apt-get install -y build-essential \
      valgrind \
      gcc \
      g++ \
      gdb \
      clang \
      make \
      ninja-build \
      cmake \
      autoconf \
      automake \
      locales-all \
      dos2unix \
      libgoogle-perftools-dev \
      google-perftools \
      tar \
      libomp-dev \
    && apt-get clean

RUN ln -s -T /usr/bin/make /usr/bin/gmake # buildkit